/* -------------------------------------------- */
import { FraggedKingdomUtility } from "./fragged-kingdom-utility.js";
import { FraggedKingdomRoll } from "./fragged-kingdom-roll-dialog.js";

/* -------------------------------------------- */
const coverBonusTable = { "nocover": 0, "lightcover": 2, "heavycover": 4, "entrenchedcover": 6};

/* -------------------------------------------- */
/* -------------------------------------------- */
/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class FraggedKingdomActor extends Actor {

  /* -------------------------------------------- */
  /**
   * Override the create() function to provide additional SoS functionality.
   *
   * This overrided create() function adds initial items 
   * Namely: Basic skills, money, 
   *
   * @param {Object} data        Barebones actor data which this function adds onto.
   * @param {Object} options     (Unused) Additional options which customize the creation workflow.
   *
   */

  static async create(data, options) {

    // Case of compendium global import
    if (data instanceof Array) {
      return super.create(data, options);
    }
    // If the created actor has items (only applicable to duplicated actors) bypass the new actor creation logic
    if (data.items) {
      let actor = super.create(data, options);
      return actor;
    }

    if ( data.type == 'character') {
      const skills = await FraggedKingdomUtility.loadCompendium("fvtt-fragged-kingdom.skills");
      data.items = skills.map(i => i.toObject());
    }
    if ( data.type == 'npc') {
      let myObj = { }
      let myId = randomID(16);
      myObj[myId] = FraggedKingdomUtility.getDefaultWeaponStat();
      let defaultWeapon = { name : 'Default Weapon', type: 'weapon', data: { weaponstats: myObj } };
      data.items = [ defaultWeapon ];
    }

    return super.create(data, options);
  }
    
  /* -------------------------------------------- */
  prepareBaseData() {
  }

  /* -------------------------------------------- */
  async prepareData() {
    
    super.prepareData();
  }

  /* -------------------------------------------- */
  prepareDerivedData() {
    if (this.type == 'character') {
      let restotal = this.data.data.level.value + 3 + this.data.data.resources.bonus;
      if ( restotal != this.data.data.resources.total) {
        this.data.data.resources.total = restotal;
        this.update( { 'data.resources.total': restotal } );
      }
      let renowntotal = this.data.data.level.value + 3 + this.data.data.renown.bonus;
      if ( renowntotal != this.data.data.renown.total) {
        this.data.data.renown.total = renowntotal;
        this.update( { 'data.renown.total': renowntotal } );
      }
      let endmax = 10 + (this.data.data.attributes.strength.value * 5) + this.data.data.endurance.bonus;
      if (endmax != this.data.data.endurance.max) {
        this.data.data.endurance.max = endmax;
        this.update( { 'data.endurance.max': endmax } );
      }
      let recovery = this.data.data.attributes.focus.value + this.data.data.recovery.bonus;
      if (recovery != this.data.data.recovery.total) {
        this.data.data.recovery.total = recovery;
        this.update( { 'data.recovery.total': recovery } );
      }  
      let bodyarmour = this.getBodyArmour() + this.data.data.bodyarmour.bonus;
      if (bodyarmour != this.data.data.bodyarmour.total) {
        this.data.data.bodyarmour.total = bodyarmour;
        this.update( { 'data.bodyarmour.total': bodyarmour } );
      }  
      let headarmour = this.getHeadArmour() + this.data.data.headarmour.bonus;
      if (headarmour != this.data.data.headarmour.total) {
        this.data.data.bodyarmour.total = headarmour;
        this.update( { 'data.headarmour.total': headarmour } );
      }  
      let momentum = this.data.data.attributes.focus.value + this.data.data.momentum.bonus;
      if (momentum != this.data.data.momentum.max) {
        this.data.data.momentum.max = momentum;
        this.update( { 'data.momentum.max': momentum } );
      }  
      let defense = this.getDefenseBase();
      if (defense != this.data.data.defense.total) {
        this.data.data.defense.total = defense;
        this.update( { 'data.defense.total': defense } );
      }  
      let vstackle = this.data.data.defense.total + this.data.data.attributes.strength.value + this.data.data.defense.vstacklebonus;
      if (vstackle != this.data.data.defense.vstackle) {
        this.data.data.defense.vstackle = vstackle;
        this.update( { 'data.defense.vstackle': vstackle } );
      }  
      if ( this.data.data.willpower.total != this.data.data.attributes.willpower.current) {
        this.data.data.willpower.total = this.data.data.attributes.willpower.current;
        this.update( { 'data.willpower.total': this.data.data.attributes.willpower.current } );
      }
    }
    if (this.type == 'holding') {
      let cargo = this.data.data.attributes.prosperity.value + this.data.data.stats.cargo.bonus;
      if (cargo != this.data.data.stats.cargo.max) {
        this.data.data.stats.cargo.max = cargo;
        this.update( { 'data.stats.cargo.max': cargo } );
      }
      let wealth = this.data.data.attributes.prosperity.value + this.data.data.attributes.security.value + this.data.data.stats.wealth.bonus;
      if (wealth != this.data.data.stats.wealth.max) {
        this.data.data.stats.wealth.max = wealth;
        this.update( { 'data.stats.wealth.max': wealth } );
      }
      let peasants = this.data.data.attributes.prosperity.value + this.data.data.attributes.loyalty.value + this.data.data.stats.peasants.bonus;
      if (peasants != this.data.data.stats.peasants.max) {
        this.data.data.stats.peasants.max = peasants;
        this.update( { 'data.stats.peasants.max': peasants } );
      }
      let soldiers = this.data.data.attributes.security.value + this.data.data.attributes.loyalty.value + this.data.data.stats.soldiers.bonus;
      if (soldiers != this.data.data.stats.soldiers.max) {
        this.data.data.stats.soldiers.max = soldiers;
        this.update( { 'data.stats.soldiers.max': soldiers } );
      }
      let levyroll = 16 - (this.data.data.stats.peasants.max) -  this.data.data.stats.levyroll.bonus;
      if (levyroll != this.data.data.stats.levyroll.max) {
        this.data.data.stats.levyroll.max = levyroll;
        this.update( { 'data.stats.levyroll.max': levyroll } );
      }
      let food = this.data.data.attributes.fields.value * 2 + this.data.data.stats.food.bonus;
      if (food != this.data.data.stats.food.max) {
        this.data.data.stats.food.max = food;
        this.update( { 'data.stats.food.max': food } );
      }
      let herbs = this.data.data.attributes.fields.value - 2 + this.data.data.stats.herbs.bonus;
      if (herbs != this.data.data.stats.herbs.max) {
        this.data.data.stats.herbs.max = herbs;
        this.update( { 'data.stats.herbs.max': herbs } );
      }
      let lumber = this.data.data.attributes.woods.value * 2 + this.data.data.stats.lumber.bonus;
      if (lumber != this.data.data.stats.lumber.max) {
        this.data.data.stats.lumber.max = lumber;
        this.update( { 'data.stats.lumber.max': lumber } );
      }
      let beasts = this.data.data.attributes.woods.value - 2 + this.data.data.stats.beasts.bonus;
      if (beasts != this.data.data.stats.beasts.max) {
        this.data.data.stats.beasts.max = beasts;
        this.update( { 'data.stats.beasts.max': beasts } );
      }
      let stone = this.data.data.attributes.hills.value * 2 + this.data.data.stats.stone.bonus;
      if (stone != this.data.data.stats.stone.max) {
        this.data.data.stats.stone.max = stone;
        this.update( { 'data.stats.stone.max': stone } );
      }
      let ore = this.data.data.attributes.hills.value - 2 + this.data.data.stats.ore.bonus;
      if (ore != this.data.data.stats.ore.max) {
        this.data.data.stats.ore.max = ore;
        this.update( { 'data.stats.ore.max': ore } );
      }
    }

    super.prepareDerivedData();
  }

  /* -------------------------------------------- */
  _preUpdate(changed, options, user) {
    if ( changed.data?.resources?.value ) {
      if ( changed.data.resources.value < 0 ) 
        changed.data.resources.value = 0;
      if ( changed.data.resources.value > this.data.data.resources.total ) 
        changed.data.resources.value = this.data.data.resources.total; 
    }
    if ( changed.data?.renown?.value ) {
      if ( changed.data.renown.value < 0 ) 
        changed.data.renown.value = 0;
      if ( changed.data.renown.value > this.data.data.renown.total ) 
        changed.data.renown.value = this.data.data.renown.total; 
    }

    super._preUpdate(changed, options, user);
  }
  /* -------------------------------------------- */
  getBackgrounds() {
    let comp = this.data.items.filter( item => item.type == 'background');
    return comp;
  }

  /* -------------------------------------------- */
  getPerks() {
    let search =(this.type == 'character') ? 'perk' : 'holdingperk';
    let comp = this.data.items.filter( item => item.type == search);
    return comp;
  }
  /* -------------------------------------------- */
  getTraits() {
    let search = 'trait';
    if ( this.type == 'character' || this.type == 'npc') {
      search = 'trait';
    } else {
      search = 'holdingtrait';
    }
    let comp = this.data.items.filter( item => item.type == search);
    return comp;
  }
  /* -------------------------------------------- */
  getComplications() {
    let comp = this.data.items.filter( item => item.type == 'complication');
    return comp;
  }
  /* -------------------------------------------- */
  getSkills() {
    let comp = this.data.items.filter( item => item.type == 'skill');
    return comp;
  }

  /* -------------------------------------------- */
  prepareSkill( item, type) {
    if (item.type == 'skill' && item.data.data.type == type) {
      item.data.data.trainedValue = (item.data.data.trained) ? 1 : -2
      item.data.data.total = item.data.data.trainedValue + item.data.data.bonus;
      item.data.data.isTrait = item.data.data.traits.length > 0;
      return item;
    }
  }

  /* -------------------------------------------- */
  async equipItem(itemId ) {
    let item = this.data.items.find( item => item.id == itemId );
    if (item && item.data.data) {
      let update = { _id: item.id, "data.equipped": !item.data.data.equipped };
      await this.updateEmbeddedDocuments('Item', [update]); // Updates one EmbeddedEntity
    }
  }

  /* -------------------------------------------- */
  getSortedSkills() {
    let comp = {};
    comp['everyday'] = this.data.items.filter( item => this.prepareSkill(item, 'everyday') );
    comp['education'] = this.data.items.filter( item => this.prepareSkill(item, 'education') );
    comp['combat'] = this.data.items.filter( item => this.prepareSkill(item, 'combat') );
    return comp;
  }
 
  /* -------------------------------------------- */
  prepareTraitSpecific( actorData, key, traitsAttr ) {
    let trait = traitsAttr.find( item => item.data.data.subtype == key); // Get the first attribute trait
    if (trait ) {
      actorData[key].traitId = trait.id;
    } else {
      actorData[key].traitId = "";
    }
  }
  /* -------------------------------------------- */
  prepareSpacecraftTraitSpecific( actorData, key, traitsAttr ) {
    let trait = traitsAttr.find( item => item.data.data.type == key); // Get the first attribute trait
    if (trait ) {
      actorData[key].traitId = trait.id;
    } else {
      actorData[key].traitId = "";
    }
  }
  /* -------------------------------------------- */
  prepareTraitsAttributes() {
    let search = (this.type == 'character') ? 'trait' : 'holdingtrait';
    let traitsAttr = this.data.items.filter( item => item.type == search);
    let actorData = this.data.data;
    
    if ( this.type == 'character') {
      for( let key in actorData.attributes) {
        this.prepareTraitSpecific( actorData.attributes, key, traitsAttr);
      }
      this.prepareTraitSpecific(actorData, "renown", traitsAttr);
      this.prepareTraitSpecific(actorData, "resources", traitsAttr);
      this.prepareTraitSpecific(actorData, "level", traitsAttr);
    } else {
      for( let key in actorData.attributes) {
        this.prepareSpacecraftTraitSpecific( actorData.attributes, key, traitsAttr);
      }
    }
  }

  /* -------------------------------------------- */
  getEquipmentSlotsBase() {
    let equipSlots = this.data.items.filter( item => item.type == 'outfit' || item.type == 'utility');
    let equipmentSlots = 0;
    for (let equip of equipSlots) {
      if ( equip.data.data.statstotal?.slots?.value && !isNaN( equip.data.data.statstotal.slots.value)) {
        equipmentSlots+= Number(equip.data.data.statstotal.slots.value);
      }
    }
    return equipmentSlots;
  }
  /* -------------------------------------------- */
  getEquipmentSlotsTotal() {
    return this.getEquipmentSlotsBase() + this.data.data.equipmentslots.bonus;
  }

  /* -------------------------------------------- */
  getSkillsTraits() { 
    let skills = this.getSkills();
    let skillsTraits = [];
    for( let skill of skills) {
      for (let trait of skill.data.data.traits) {
        trait.associatedSkill = skill.name;
      }
      skillsTraits = skillsTraits.concat( skill.data.data.traits );
    }
    //console.log("Consolidated skills", skillsTraits);
    return skillsTraits;
  } 

  /* -------------------------------------------- */
  getTrait( traitId  ) {
    let trait = this.data.items.find( item => item.id == traitId );
    return trait;
  }
  
  /* -------------------------------------------- */
  compareName( a, b) {
    if ( a.name < b.name ) {
      return -1;
    }
    if ( a.name > b.name ) {
      return 1;
    }
    return 0;
  }
  /* -------------------------------------------- */
  getLanguages() {
    return this.data.items.filter( item => item.type == 'language'  );
  }
  /* -------------------------------------------- */
  getStrongHits() {
    return this.data.items.filter( item => item.type == 'stronghit'  );
  }
  /* ------------------------------------------- */
  getUtilities() {
    return this.data.items.filter( item => item.type == 'utility'  );
  }
  /* ------------------------------------------- */
  getEquipments() {
    return this.data.items.filter( item => item.type == 'utility' || item.type == 'outfit' || item.type == "weapon" || item.type == "equipment");
  }
  
  /* -------------------------------------------- */
  getRaces( ) {
    return this.data.items.filter( item => item.type == 'race' );
  }

  /* -------------------------------------------- */
  getWeapons() {
    let weapons = this.data.items.filter( item => item.type == 'weapon' );
    return weapons;
  }

  /* -------------------------------------------- */
  getOutfits() {
    return this.data.items.filter( item => item.type == 'outfit' );
  }

  /* -------------------------------------------- */
  getActiveEffects(matching = it => true) {
    let array = Array.from(this.getEmbeddedCollection("ActiveEffect").values());
    return Array.from(this.getEmbeddedCollection("ActiveEffect").values()).filter(it => matching(it));
  }
  /* -------------------------------------------- */
  getEffectByLabel(label) {
    return this.getActiveEffects().find(it => it.data.label == label);
  }
  /* -------------------------------------------- */
  getEffectById(id) {
    return this.getActiveEffects().find(it => it.id == id);
  }

  /* -------------------------------------------- */
  getAttribute( attrName ) {
    for( let key in this.data.attributes) {
      let attr = this.data.data.carac[key];
      if (attr.label.toLowerCase() == attrName.toLowerCase() ) {
        return deepClone(categ.carac[carac]);
      }
    }
  }

  /* -------------------------------------------- */
  async equipGear( equipmentId ) {    
    let item = this.data.items.find( item => item.id == equipmentId );
    if (item && item.data.data) {
      let update = { _id: item.id, "data.equipe": !item.data.data.equipe };
      await this.updateEmbeddedDocuments('Item', [update]); // Updates one EmbeddedEntity
    }
  }

  /* -------------------------------------------- */
  saveRollData( rollData) {
    this.currentRollData = rollData;
  }
  /* -------------------------------------------- */
  getRollData( ) {
    return this.currentRollData;
  }
  
  /* -------------------------------------------- */
  getInitiativeScore( )  {
    if ( this.type == 'character') {
      return this.data.data.attributes.intelligence.current + (this.data.data.attributes.reflexes.current/10);
    } 
    return 0.0;
  }

  /* -------------------------------------------- */
  getDefenseBase() {
    if (this.type == 'character') {
      let outfitBonus = 0;
      let outfits = this.data.items.filter( item => (item.type == 'outfit' || item.type == 'utility') && item.data.data.equipped );
      for (let item of outfits) {
        if ( !isNaN(item.data.data.statstotal?.defence?.value)) {
          outfitBonus += Number(item.data.data.statstotal.defence.value);
        }
      }
      let coverbonus = 0;
      if ( this.data.data.defense.cover == "lightcover") coverbonus = this.data.data.attributes.intelligence.value;
      if ( this.data.data.defense.cover == "heavycover") coverbonus = this.data.data.attributes.intelligence.value*2;
      if ( this.data.data.defense.cover == "entrenchedcover") coverbonus = this.data.data.attributes.intelligence.value*3;
      return 10 + this.data.data.attributes.reflexes.current + outfitBonus + coverbonus + this.data.data.defense.bonus;
    }
    return 0;
  }

  /* -------------------------------------------- */
  getBodyArmour( ) {
    if (this.type == 'character') {
      let armour = 0;
      let outfits = this.data.items.filter( item => (item.type == 'outfit' || item.type == 'utility') && item.data.data.equipped ) || []      
      for (let item of outfits) {
        if ( !isNaN(item.data.data.statstotal?.armourbody?.value)) {
          armour += Number(item.data.data.statstotal.armourbody.value);
        }
      }
      return armour;
    }
    return 0;
  }
  /* -------------------------------------------- */
  getHeadArmour( ) {
    if (this.type == 'character') {
      let armour = 0;
      let outfits = this.data.items.filter( item => (item.type == 'outfit' || item.type == 'utility') && item.data.data.equipped ) || []
      for (let item of outfits) {
        if ( !isNaN(item.data.data.statstotal?.armourhead?.value)) {
          armour += Number(item.data.data.statstotal.armourhead.value);
        }
      }
      return armour;
    }
    return 0;
  }
  /* -------------------------------------------- */
  getTradeGoods( ) {
    let tradeGoods = this.data.items.filter( item => item.type == 'tradegood' );
    for (let good of tradeGoods) {
      good.cargoSpace = Math.ceil(good.data.data.tradebox / 4); 
    }
    return tradeGoods;

  }
  /* -------------------------------------------- */
  getResearch( ) {
    let research = this.data.items.filter( item => item.type == 'research' );
    return research;

  }
  /* -------------------------------------------- */
  getSubActors() {
    let subActors = [];
    for (let id of this.data.data.subactors) {
      let subActor = game.actors.get(id);
      let pushSubActor = duplicate(subActor);
      pushSubActor.weapons = subActor.getWeapons();
      subActors.push(pushSubActor);
    }
    return subActors;
  }
  /* -------------------------------------------- */
  async addSubActor( subActorId) {
    let subActors = duplicate( this.data.data.subactors);
    subActors.push( subActorId);
    await this.update( { 'data.subactors': subActors } );
  }
  /* -------------------------------------------- */
  async delSubActor( subActorId) {
    let newArray = [];
    for (let id of this.data.data.subactors) {
      if ( id != subActorId) {
        newArray.push( id);
      }
    }
    await this.update( { 'data.subactors': newArray } );
  }

  /* -------------------------------------------- */
  decrementMomentum( momentumValue) {
    if ( this.type  == 'character' && !isNaN(momentumValue)) {
      let newMomentum = this.data.data.momentum.value - Number(momentumValue);
      this.update( {'data.momentum.value':  newMomentum } );
    }
  }
  /* -------------------------------------------- */
  decrementWillpower() {
    if (this.type == 'character' && this.data.data.willpower.current > 0) {
      this.data.data.willpower.current--;
      this.update( {'data.willpower.current':  this.data.data.willpower.current } );
      return this.data.data.willpower.current;
    }
    return false;
  }
  /* -------------------------------------------- */
  hasWillpower() {
    if (this.type == 'character' && this.data.data.willpower.current > 0) {
      return this.data.data.willpower.current;
    }
    return false;
  }
  /* -------------------------------------------- */
  async rollSkill( competenceId ) {
    let skill = this.data.items.find( item => item.type == 'skill' && item.id == competenceId);
    if (skill) {
      let rollData = {
        mode: "skill",
        alias: this.name, 
        actorType: this.type,
        actorImg: this.img,
        actorId: this.id,
        img: skill.img,
        hasWillpower: this.hasWillpower(),
        rollMode: game.settings.get("core", "rollMode"),
        title: `Skill ${skill.name} : ${skill.data.data.total}`,
        skill: skill,
        attrList: duplicate(this.data.data.attributes),
        attrSkill: skill.data.data.defaultattr,
        skillBonus: 0,
        totalSkill: skill.data.data.total,
        optionsBonusMalus: FraggedKingdomUtility.buildListOptions(-6, +6),
        bonusMalus: 0,
        optionsDifficulty: FraggedKingdomUtility.buildDifficultyOptions( ),
        difficulty: 0,
        useToolbox: false,
        useDedicatedworkshop: false,
        toolsAvailable: skill.data.data.toolbox || skill.data.data.useDedicatedworkshop
      }
      let rollDialog = await FraggedKingdomRoll.create( this, rollData);
      console.log(rollDialog);
      rollDialog.render( true );
    } else {
      ui.notifications.warn("Skill not found !");
    }
  }

  /* -------------------------------------------- */
  async rollGenericSkill( ) {
    let rollData = {
      mode: "genericskill",
      alias: this.name, 
      actorType: this.type,
      actorImg: this.img,
      actorId: this.id,
      img: this.img,
      hasWillpower: this.hasWillpower(),
      rollMode: game.settings.get("core", "rollMode"),
      title: `Generic Skill roll`,
      optionsBonusMalus: FraggedKingdomUtility.buildListOptions(-6, +6),
      bonusMalus: 0,
      optionsDifficulty: FraggedKingdomUtility.buildDifficultyOptions( ),
      difficulty: 0
    }
    let rollDialog = await FraggedKingdomRoll.create( this, rollData);
    console.log(rollDialog);
    rollDialog.render( true );
  }

  /* -------------------------------------------- */
  async rollWeapon( weaponId, statId ) {
    let weapon = this.data.items.find( item => item.id == weaponId);
    console.log("WEAPON :", weaponId, weapon );
        
    if ( weapon ) {
      
      let rollData = {
        mode: 'weapon',
        actorType: this.type,
        alias: this.name, 
        actorId: this.id,
        img: weapon.img,
        hasWillpower: this.hasWillpower(),
        rollMode: game.settings.get("core", "rollMode"),
        title: "Attack : " + weapon.name,
        weapon: weapon,
        statId: statId,
        weaponStat: weapon.data.data.weaponstats[statId].data.statstotal,
        optionsBonusMalus: FraggedKingdomUtility.buildListOptions(-6, +6),
        bonusMalus: 0,
        momentumValue: 0,
        bodiesValue: 0,
        optionsDifficulty: FraggedKingdomUtility.buildDifficultyOptions( )
      }
      // Add skill for character only
      if (this.type == 'character') {
        let weaponSkills = this.data.items.filter( item => item.type == 'skill' && item.data.data.type == 'combat');
        rollData.weaponSkills =  weaponSkills;
        let combatSkill = weaponSkills[0];
        if ( weapon.data.data.defaultskill != "") {
          combatSkill = this.data.items.find( item => item.type == 'skill' && item.data.data.type == 'combat' && item.name == weapon.data.data.defaultskill);
        }
        rollData.skillId = combatSkill.id;
        rollData.skill = combatSkill;
        rollData.optionsMomentum = FraggedKingdomUtility.buildListOptions(0, this.data.data.momentum.value);
      }
      let rollDialog = await FraggedKingdomRoll.create( this, rollData);
      console.log("WEAPON ROLL", rollData);
      rollDialog.render( true );
    } else {
      ui.notifications.warn("Weapon not found !", weaponId);
    }
  }

}
