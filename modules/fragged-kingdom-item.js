import { FraggedKingdomUtility } from "./fragged-kingdom-utility.js";

/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class FraggedKingdomItem extends Item {

  _onCreate(data, options, userId) {
    if ( data.type == 'weapon') {
      if ( Object.keys(data.data.weaponstats).length == 0) {
        let myId = randomID(16);
        let defaultStatData = FraggedKingdomUtility.getDefaultWeaponStat();
        defaultStatData._id = myId;
        let myObj = {}
        myObj[myId] = defaultStatData;
        this.update( {'data.weaponstats': myObj} );
      }
    }
    super._onCreate(data, options, userId);
  }

}
