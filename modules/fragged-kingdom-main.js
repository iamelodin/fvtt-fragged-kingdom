/**
 * FraggedKingdom system
 * Author: Uberwald
 * Software License: Prop
 */

/* -------------------------------------------- */

/* -------------------------------------------- */
// Import Modules
import { FraggedKingdomActor } from "./fragged-kingdom-actor.js";
import { FraggedKingdomItemSheet } from "./fragged-kingdom-item-sheet.js";
import { FraggedKingdomActorSheet } from "./fragged-kingdom-actor-sheet.js";
import { FraggedKingdomHoldingSheet } from "./fragged-kingdom-holding-sheet.js";
import { FraggedKingdomNPCSheet } from "./fragged-kingdom-npc-sheet.js";
import { FraggedKingdomUtility } from "./fragged-kingdom-utility.js";
import { FraggedKingdomCombat } from "./fragged-kingdom-combat.js";
import { FraggedKingdomItem } from "./fragged-kingdom-item.js";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

/************************************************************************************/
Hooks.once("init", async function () {
  console.log(`Initializing Fragged Kingdom`);

  /* -------------------------------------------- */
  // preload handlebars templates
  FraggedKingdomUtility.preloadHandlebarsTemplates();

  /* -------------------------------------------- */
  // Set an initiative formula for the system 
  CONFIG.Combat.initiative = {
    formula: "1d6",
    decimals: 1
  };

  /* -------------------------------------------- */
  game.socket.on("system.fvtt-fragged-kingdom", data => {
    FraggedKingdomUtility.onSocketMesssage(data);
  });

  /* -------------------------------------------- */
  // Define custom Entity classes
  CONFIG.Combat.documentClass = FraggedKingdomCombat;
  CONFIG.Actor.documentClass = FraggedKingdomActor;
  CONFIG.Item.documentClass = FraggedKingdomItem;
  CONFIG.FraggedKingdom = {
  }

  /* -------------------------------------------- */
  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("fvtt-fragged-kingdom", FraggedKingdomActorSheet, { types: ["character"], makeDefault: true });
  Actors.registerSheet("fvtt-fragged-kingdom", FraggedKingdomHoldingSheet, { types: ["holding"], makeDefault: false });
  Actors.registerSheet("fvtt-fragged-kingdom", FraggedKingdomNPCSheet, { types: ["npc"], makeDefault: false });

  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("fvtt-fragged-kingdom", FraggedKingdomItemSheet, { makeDefault: true });

  FraggedKingdomUtility.init();
  
});

/* -------------------------------------------- */
function welcomeMessage() {
  ChatMessage.create({
    user: game.user.id,
    whisper: [game.user.id],
    content: `<div id="welcome-message-fragged-kingdom"><span class="rdd-roll-part">Welcome !</div>
    ` });
}

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */
Hooks.once("ready", function () {

  FraggedKingdomUtility.ready();
  // User warning
  if (!game.user.isGM && game.user.character == undefined) {
    ui.notifications.info("Warning ! No character linked to your user !");
    ChatMessage.create({
      content: "<b>WARNING</b> The player  " + game.user.name + " is not linked to a character !",
      user: game.user._id
    });
  }
  welcomeMessage();
});

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */
Hooks.on("chatMessage", (html, content, msg) => {
  if (content[0] == '/') {
    let regExp = /(\S+)/g;
    let commands = content.toLowerCase().match(regExp);
    console.log(commands);
  }
  return true;
});
